<div class="install-only-stable" markdown="1">
On openSUSE, you can install PeerTube via our RPM package repository.

```bash
sudo zypper ar -f https://pkg.rigelk.eu/rpm/ peertube
sudo zypper in peertube
```

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no openSUSE packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>
