<div class="alert alert-warning" role="alert">
  <strong>Warning</strong>: the debian package is currently having issues and is only providing
  an outdated version of PeerTube. We are resolving the issue with the package maintainer, and
  recommend you to intsall the package via the <a href="https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md">reference production guide</a>.
</div>

<div class="install-only-beta" markdown="1">
On Debian or Ubuntu Linux, you can install Yarn, Node and PeerTube in one shot via this __community maintained__ repository. You will first need to configure the repository:

```sh
curl -sS https://pkg.rigelk.eu/pubkey.gpg | sudo apt-key add -
echo "deb https://pkg.rigelk.eu/debian/ stable main" | sudo tee /etc/apt/sources.list.d/peertube.list
```

Then you can simply:

```sh
sudo apt-get update && sudo apt-get install peertube
```

#### Configuration

You now have to [configure the database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database)
and credentials to it in the configuration file of PeerTube
in `/etc/peertube/production.yaml`.

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Debian packages available for RC or nightly builds of PeerTube. Please use the [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md).
</div>
