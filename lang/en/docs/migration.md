---
id: docs_migration
guide: docs_migration
layout: guide
timer: false
---

This guide describes how to migrate your PeerTube instance from one server to
another. It assumes you have root access to both servers, and some coffee. It
may result in some downtime.

<div class="alert alert-warn" role="alert">
   <strong>Note:</strong> do not modify anything on the old server until you have successfully migrated the instance.
</div>

## Basic steps

1. Setup a new PeerTube server using the [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md)
2. Stop PeerTube on the old server
3. Dump and load the PostgreSQL database using the instructions below
4. Copy or synchronize the `storage/` files using the instructions below (¹)
5. Start PeerTube on the new server
6. Update your DNS settings to point to the new server
7. Update or copy your Nginx configuration, re-run LetsEncrypt as necessary (²)
8. Enjoy your new PeerTube server!

## Detailed steps

### What data needs to be migrated

At a high level, you will need to copy over the following:

- The `/var/www/peertube/storage`, which contains videos, thumbnails, previews…
- The `/var/www/peertube/config` file, which contains the configuration
- The PostgreSQL database (using [pg_dump](https://www.postgresql.org/docs/9.6/static/backup-dump.html))

Less crucially, you will probably also want to copy the following for convenience:

- The nginx configuration (under `/etc/nginx/sites-available/peertube`)
- The systemd config files or startup scripts, which may contain your server tweaks and customizations

### Dump and load PostgreSQL

Run this as the `peertube` user on the old server:

```bash
sudo -u peertube pg_dump -Fc peertube_prod > /tmp/peertube_prod-dump.db
```

Copy the `/tmp/peertube_prod-dump.db` file over, using `scp` or `rsync` at your
convenience:

```bash
scp /tmp/peertube_prod-dump.db user@new.server:/tmp
```

Then on the new system, run:

```bash
sudo -u postgres pg_restore -c -C -d postgres /tmp/peertube_prod-dump.db
```

You might see warnings that you can [safely ignore](https://confluence.atlassian.com/bamkb/errors-or-warnings-appear-when-importing-postgres-database-dump-829036698.html).

### Copy `storage/` files

This will probably take a long time! You want to avoid copying unnecessarily,
so using `rsync` is recommended. But you can use `scp` too.

On your old machine, as the `peertube` user, run:

```bash
rsync -avz ~/storage/ peertube@example.com:~/storage/
```

You will need to re-run this if any of the files on the old server change. That's
why it's better to use `rsync`.

(¹) you could probably finish an rsync run while the old server is still
running, and then just re-synchronize after shutting it down. Or use external
storage if you can to avoid this step (then you just have to configure the
mount in fstab).

If you still want to use `scp`:

```bash
scp -r ~/storage peertube@example.com:~/storage
```

You can also copy over any other important files, such as `config/` and the
nginx, systemd configuration or startup script.

#### (²) Let's Encrypt

You should copy the Let's Encrypt certs over from the old server instead of
request a new cert.

Requesting new certs takes longer, and might fail for some reason. Better if
you have time to fix that after the migration is complete.

### During the migration

You may want to configure nginx to send a 503 (_service unavailable_)
instead of a 501 (_bad gateaway_), and answer with a custom `500.html`.

You will probably want to set the DNS TTL to the lowest possible value about
a day in advance. Set it to a few minutes if you can. That ensures the DNS
update propagates as quickly as possible once you point it to the new IP address,
thus less downtime.

### After the migration

You can check [whatsmydns.net](https://www.whatsmydns.net/) to see the progress
of DNS propagation. To jumpstart the process, you can always edit your own
`/etc/hosts` file to point to your new server so you can start playing around
with it early and check if all is all right.

If everything is right, you can safely shut down the old server.

## Acknowledgements

- Thanks to the [Mastodon](https://joinmastodon.org/) team for their migration guide, which largely inspired this guide.
- Thanks to [@Nutomic](https://framagit.org/Nutomic) for his comments.
